let count = 0;
while(count !== 10){
	console.log("While: " + count);
// iteration - it increases the value of count after every iteration to stop lthe loop when it reaches 5.
	

	count++;
}

// [SECTION] While Loop

// If the condition evaluates to true, the statements inside the code blcok will executed

/*
	Syntax 
	while (expression/condition){
		statement
	}
*/

// While the value of count is not equal to 0
// iteration - it increases the value of count after every iteration to stop the loop when it reaches 5

	// ++ - increment, -- decrement

//section Do While Loop

//a do-while loop works a lot like the while loop
//but unlike while loops, do-while loops guarantee that the code will be executed at least once.

/* syntax:

	do{
		statement
	} while ( expression/condition)

*/

let number = Number(prompt("Give me a number"));
do{
	 console.log("do while: " + number);

	 number -= 5
} while (number < 10);

//Sections for Loops
// A.FOR loop is more flexible than while and do-while loops


// Syntax
/* for (initialization; expression/condition; final expression){
		statement

	}
*/

for (let count = 0; count <= 10;  count++ ) {
	console.log(count);
}
//mas gamit 

let myString = "ALEX";//pwede prompt

// Characters in strings may be counted using the .length property.
console.log(myString.length);
console.log(myString[0]);//A
console.log(myString[1]);//L
console.log(myString[2]);//E
console.log(myString[3]);//X

// will create a loop that will print out the individual letters of a variable

for (let x = 0; x < myString.length; x++){

	// the current value of my string is printed out using it's index value
	console.log(myString[x])
}




//Changing of vowels using loops

// if the character of you name is a vowel letter, instead of displaying the character, it displays a numhber
let myName = "DelIzo";

for (let i = 0; i < myName.length; i++){
	if (
			myName[i].toLowerCase() == "a" || 
			myName[i].toLowerCase() == "i" || 
			myName[i].toLowerCase() == "u" || 
			myName[i].toLowerCase() == "e" || 
			myName[i].toLowerCase() == "o" 

		){
			console.log(3);

	} else {
		console.log(myName[i]);
	}

}