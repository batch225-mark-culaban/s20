// number 1

let number = prompt("Enter the number: ");

console.log("The number you provided is " + number);

// Soln 1 [while loop]

// while (number > 50){
// 	if(number % 10 == 0){
// 		console.log("the number is divisible by 10. skipping a number.")
// 	}
// 	else if (number % 5 == 0) {
// 		console.log(number);
// 	}

// 	number -=1;
// }

// Soln 2 [do-While loop]

// do {
// 	if (number % 10 === 0) {
// 		console.log("The number is divisible by 10. Skipping the number.");
// 	}
// 	else if (number <= 50) {
// 	 	console.log("The current value is at 50. Terminating the loop.");
// 	}
// 	else console.log(number);
// 		number -= 5;

// } while (number >= 50);


// Soln 3 [for Loop]

for(let getValue = number; number >= 0; number--){
	if (number % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
	}

	else if (number % 5 === 0) {
		console.log(number)
	}

	else if ( number <= 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break;
	} 
}


// number 2


let name = "supercalifragilisticexpialidocious".toLowerCase();
	console.log(name);

let result = "";

for(let i = 0; i < name.length; i++) {

	if(name[i] === 'a' || name[i] === 'e' || name[i] === 'i' || name[i] === 'o' || name[i] === 'u') {

		continue;
		}

		else {
			result += name[i];
		}
		
}

console.log(result);






console.log("Using continue");

for (let = count = 0; count <= 10; count++) {
    if (count === 5){
        continue;
    }

    console.log("Continue " + count);

}

console.log("Using break");

for (let = count = 0; count <= 10; count++) {
    if (count === 5){
        break;
    }

    console.log("Break " + count);

}